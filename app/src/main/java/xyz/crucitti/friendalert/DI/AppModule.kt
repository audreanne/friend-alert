package xyz.crucitti.friendalert.DI

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import xyz.crucitti.friendalert.data.AppDatabase
import xyz.crucitti.friendalert.data.DeviceContacts
import xyz.crucitti.friendalert.repository.ContactRepository

val appModule = module {
    single { AppDatabase.getInstance(get()) }
    single { AppDatabase.getInstance(get()).contactDao() }
    single { AppDatabase.getInstance(get()).settingsDao() }
    single { androidContext().contentResolver }
    single { DeviceContacts(get(), get()) }
    single { ContactRepository(get(), get()) }
}