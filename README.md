# FriendAlert: Keep Your Friendships Thriving, No Matter How Far Apart

Life's adventures lead us to friendships that cross cities, countries, and even continents. While
these connections add immeasurable value to our lives, keeping them vibrant across distances can be
a challenge. Born from this universal struggle, FriendAlert is your go-to solution for keeping those
important friendships alive and well.

🌍 **Your Central Hub for Global Friendships**  
Whether your friendships were formed on the playground or during an international journey,
FriendAlert functions as your unified platform for sustaining these invaluable bonds. With
FriendAlert, neither miles nor time zones can weaken your connections.

🗣️ **Foster Authentic Connections: More than Just Check-ins**  
Social media may offer cursory updates about people's lives, but it's no substitute for genuine
conversation. FriendAlert encourages you to engage in meaningful dialogues that go beyond the
superficial.

# Functions

The idea is to keep most of the information within the contact information so that it can easily be
exported and transferred as a VCF.
Not everything is possible in this way and thus some information is stored internally in the
application.
This application is not meant to be a contact editor, use your default phone app for setting contact
name, picture and so on.
The reminder feature is pretty simple: you set up how often you want to contact someone and if you
don't update the last encounter with such person within that timeframe the application will notify
you. Everything has to be set manually, there is no automation. Eventually I'd like the application
to automatically register phone calls but that's as far as automating it I feel it's useful. The
main point of the app is to have a place to remind you about people, you're still the one keeping
track of your friends and deciding what to do. Too much automation may not be the best idea.

# Features I wish to add

- Notes about people - A simple text to remind you important things about them, this is the normal
  notes field present in VCF which you can also edit through your normal contacts app.
- Places where a person has lived - This is useful to keep track of all the places your friends know
  moderately well. The main idea behind this is that if you are travelling somewhere you can search
  such place in the app and find all people that have some knowledge of the place, so that you can
  ask advice or help in finding accommodation.
- Meeting log - A short description of when you met with your friend, useful if they told you
  something important about them which you feel you should remember. I'm still unsure whether this
  could be stored in the contact VCF or should be separated.
- Some kind of visual representation of how often you met a particular person.

# A Personal Perspective

I created FriendAlert with the aim of maintaining contact with the people who matter most to me.
While the app's utility can vary, here are some personal guidelines I follow:

- Use FriendAlert to track those you find you're losing touch with, not the friends you see
  regularly.
- Although in-person meetings are irreplaceable, phone or video calls serve as excellent
  alternatives when that's not feasible.
- Texting, though convenient, falls short in conveying emotional nuance and should not be your
  primary mode of communication.
- Engaging via social media—such as liking photos or commenting—simply doesn’t cut it when it comes
  to nurturing real friendships.

# Elevate Your Social Ecosystem

FriendAlert emerged from a personal need for an accessible, budget-friendly way to sustain my social
circle. While platforms like nouri.ai have their merits, FriendAlert offers a free and user-friendly
alternative.

Sharing good moments is nice. Let's enrich not just our conversations, but our lives and communities
through stronger, more meaningful connections.

# Interesting reads

Have a look at the following article, it's a quick read about how you can use a spreadsheet to keep
track of your friendships and how that can be useful:
https://www.npr.org/2019/08/29/755390432/want-stronger-friendships-pull-out-your-notepad

# Contributing

As of now most of the functions of the application still have to be well defined and it has to be
decided how to structure the code base. As such, I do not believe that punctual code contributions
will be easily accepted unless on vary minor aspects of the code. In any way just send me a message
if you want to contribute. What is most pressing is improving the interface and design of the app:
better colors, a better icon and designing actually good layouts to make it more usable.